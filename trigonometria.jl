# MAC0110 - MiniEP7
# Luca Assumpcao Dillenburg - 11796580

iteracoes = 10

function tayler_sin(x)
	sin = x
	for m = 0:iteracoes - 1
		impar = 3 + 2 * m
		sin += big(((m % 2 == 0) ? -1 : 1) * (x^impar) / factorial(big(impar)))
	end
	return sin
end

function tayler_cos(x)
    cos = 1
	for m = 0:iteracoes - 1
		par = 2 + 2 * m
		cos += big(((m % 2 == 0) ? -1 : 1) * (x^par) / factorial(big(par)))
	end
	return cos
end

function tayler_tan(x)
    tan = x
	for m = 0:iteracoes - 1
		impar = 3 + 2 * m
		n = m + 2

		tan += big((2^(2 * n) * (2^(2 * n) - 1) * bernoulli(n) * x^impar) / factorial(big(2 * n)))
	end
	return tan
end

function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0:n
		A[m + 1] = 1 // (m + 1)
		for j = m:-1:1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return abs(A[1])
end

function quaseigual(v1, v2)
	return abs(v1 - v2) < 0.001
end

function check_sin(value, rad)
	return quaseigual(value, tayler_sin(rad))
end

function check_cos(value, rad)
	return quaseigual(value, tayler_cos(rad))
end

function check_tan(value, rad)
	return quaseigual(value, tayler_tan(rad))
end

function test()
	if (!quaseigual(0.1, 0.100000001) || quaseigual(0.001, 0.002) || !quaseigual(0.001, 0.0018))
		print("Quase igual esta errado")
	end

	if (!check_sin(0.5, pi / 6)) || !check_sin(0.866666, pi / 3)
		print("Sin esta errado")
	end

	if (!check_cos(0.866666, pi / 6) || !check_cos(0.5, pi / 3))
		print("Cos esta errado")
	end

	if (!check_tan(0.57735, pi / 6) || !check_tan(1.7320, pi / 3))
		print("Tan esta errada")
	end
end
